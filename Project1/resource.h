//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Project1.rc
//
#define IDC_MYICON                      2
#define IDD_PROJECT1_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_PROJECT1                    107
#define IDI_SMALL                       108
#define IDC_PROJECT1                    109
#define IDR_MAINFRAME                   128
#define IDB_PNG1                        131
#define IDB_BITMAP1                     145
#define IDB_BITMAP7                     152
#define ID_FILE_NEW                     32771
#define ID_FILE_OPEN                    32772
#define ID_FILE_SAVE                    32773
#define ID_DRAW_COLOR                   32774
#define ID_DRAW_FONT                    32775
#define ID_DRAW_LINE                    32776
#define ID_DRAW_RECTANGLE               32777
#define ID_DRAW_ELLIPSE                 32778
#define ID_DRAW_TEXT                    32779
#define ID_WINDOW_TIDE                  32781
#define ID_WINDOW_CASCADE               32782
#define ID_WINDOW_CLOSEALL              32783
#define ID_DRAW_SELECTOBJECT            32784
#define ID_CLOSEALL                     32785
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        153
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
