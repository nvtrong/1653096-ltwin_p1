// Project1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Project1.h"
#include <commctrl.h>

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

TCHAR szDrawWnd[] = TEXT("DRAW");
HWND hwndMDIClient = NULL;
HWND hFrameWnd = NULL;
HWND hToolBarWnd = NULL;

COLORREF rgbCurrentColor = RGB(0, 0, 0);

int nBitmapClass = 1;
int curCheck;

HIMAGELIST g_hImageList = NULL;

struct CHILD_WND_DATA
{
	BYTE wndType;
	HWND hWnd;
	COLORREF rgbColor;
	LOGFONT logFont;
};

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


void InitFrameWnd(HWND);
LRESULT CALLBACK	DrawWndProc(HWND, UINT, WPARAM, LPARAM);
void initChildWnd(HWND hWnd, int nType);
void onNewDrawWnd(HWND hWnd);
LRESULT onCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam);
void initChoseColor(HWND hWnd);
void initChooseFont(HDC hdc, HWND hWnd);

void crtToolBar(HWND hWnd);
void addDrawButton(HWND hWnd);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PROJECT1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PROJECT1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    RegisterClassExW(&wcex);


	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 4;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PROJECT1);
	wcex.lpszClassName = szDrawWnd;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassExW(&wcex);

	return RegisterClassExW(&wcex);

}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
	
		InitFrameWnd(hWnd);
		crtToolBar(hWnd);
		addDrawButton(hWnd);
		return 0;

	}
	break;

	case WM_COMMAND:
	{
		onCommand(hWnd, message, wParam, lParam);
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
			SetWindowPos(hwndMDIClient, hwndMDIClient, 0, 32, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
			ShowWindow(hwndMDIClient, SW_SHOW);
			return 0;
        }
        break;
	case WM_SIZE:
	{
		UINT x, y;
		x = LOWORD(lParam);
		y = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 0, x, y,TRUE);
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
	case WM_MOVE:
	{
		
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefFrameProc(hWnd, hwndMDIClient,message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}



void InitFrameWnd(HWND hWnd)
{
	hFrameWnd = hWnd;
	// Parse the menu selections:
	CLIENTCREATESTRUCT cs;
	cs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	cs.idFirstChild = 500000;
	if (hWnd == NULL) {
		MessageBox(hWnd, _T("hWnd NULL"), _T("Null"), 0);
	}
	hwndMDIClient = CreateWindow(L"MDIClient",
		(LPCTSTR)NULL,
		WS_CHILD | WS_CLIPCHILDREN,
		0, 0, 0, 0,
		hWnd,
		(HMENU)NULL,
		hInst,
		(LPVOID)&cs);
	if (hwndMDIClient == NULL) {
		MessageBox(hWnd, _T("Loi"), _T("Loi"), 0);
	}
	ShowWindow(hwndMDIClient, SW_SHOW);


	HMENU hMenu = GetMenu(hWnd);
	CheckMenuItem(hMenu, ID_DRAW_LINE,MF_CHECKED|MF_BYCOMMAND);
	curCheck = ID_DRAW_LINE;

}
LRESULT onCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId = LOWORD(wParam);
	switch (wmId)
	{
	case ID_FILE_NEW:
		onNewDrawWnd(hWnd);
		break;
	case ID_WINDOW_TIDE:
		SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0);
		break;
	case ID_WINDOW_CASCADE:
		SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
		break;
	case ID_WINDOW_CLOSEALL:
		EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
		break;
	case ID_FILE_OPEN:
		MessageBox(hwndMDIClient, _T("Ban da chon Open"), _T("Open dialog"), 0);
		break;
	case ID_FILE_SAVE:
		MessageBox(hwndMDIClient, _T("Ban da chon Save"), _T("Save dialog"), 0);
		break;
	case ID_DRAW_COLOR:
		initChoseColor(hwndMDIClient);
		break;
	case ID_DRAW_FONT:
	{
		HDC hdc = GetDC(hwndMDIClient);
		initChooseFont(hdc, hwndMDIClient);
		ReleaseDC(hwndMDIClient, hdc);
		break;
	}
	case ID_DRAW_ELLIPSE:
	case ID_DRAW_LINE:
	case ID_DRAW_RECTANGLE:
	case ID_DRAW_TEXT:
	case ID_DRAW_SELECTOBJECT:
	{
		HMENU hMenu = GetMenu(hWnd);
		CheckMenuItem(hMenu, curCheck, MF_UNCHECKED | MF_BYCOMMAND);
		curCheck = wmId;
		CheckMenuItem(hMenu, curCheck, MF_CHECKED | MF_BYCOMMAND);
	}
	break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;	
	default:
		return DefMDIChildProc(hwndMDIClient, message, wParam, lParam);
	}
}

LRESULT CALLBACK	DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		nBitmapClass++;
		initChildWnd(hWnd, 1);
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CLOSE:
	{
		nBitmapClass--;
		SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)(hWnd), 0L);
	}
	break;
	//case WM_MDIACTIVATE:
	//{
	//	onMDIActivate(hWnd);
	//}
	//break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
}

void initChildWnd(HWND hWnd, int nType)
{
	CHILD_WND_DATA *wndData;
	wndData = (CHILD_WND_DATA*)VirtualAlloc(NULL, sizeof(CHILD_WND_DATA), MEM_COMMIT, PAGE_READWRITE);
	wndData->wndType = nType;
	wndData->rgbColor = RGB(0, 0, 0);
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData) == 0)
		if (GetLastError() != 0)
		{
			int a = 0;
		}
}

void onNewDrawWnd(HWND hWnd)
{
	MDICREATESTRUCT chld;
	chld.szClass = szDrawWnd;
	wsprintf(szTitle, L"Noname %d", nBitmapClass);
	chld.szTitle = szTitle;
	chld.hOwner = hInst;
	chld.x = CW_USEDEFAULT;
	chld.y = CW_USEDEFAULT;
	chld.cx = CW_USEDEFAULT;
	chld.cy = CW_USEDEFAULT;
	chld.style = 0;
	chld.lParam = 0;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&chld);
}

LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)(hChildWnd), 0L);
	return 1;
}


void initChoseColor(HWND hWnd)
{
	CHOOSECOLOR cHc;
	COLORREF arcCustClr[16];
	DWORD rbgCurrent = RGB(255, 0, 0);

	ZeroMemory(&cHc, sizeof(CHOOSECOLOR));
	cHc.lStructSize = sizeof(CHOOSECOLOR);
	cHc.hwndOwner = hWnd;
	cHc.lpCustColors = (LPDWORD)arcCustClr;
	cHc.rgbResult = rbgCurrent;
	cHc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cHc))
	{
		HBRUSH hbrs;
		hbrs = CreateSolidBrush(cHc.rgbResult);
		rgbCurrentColor = cHc.rgbResult;

	}
	else
		MessageBox(hwndMDIClient,_T("Can't open ChooseColor"), _T("Error"), 0);

	
}

void initChooseFont(HDC hdc,HWND hWnd)
{
	CHOOSEFONT cf;
	LOGFONT lf;
	HFONT hFnew, hFOld;
	DWORD rgbPrev;
	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &lf;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	if (ChooseFont(&cf))
	{
		hFnew = CreateFontIndirect(cf.lpLogFont);
		hFOld = (HFONT)SelectObject(hdc, hFnew);
		rgbPrev = SetTextColor(hdc, cf.rgbColors);
		TextOut(hdc, 50, 50, _T("this text use seclected font and color"), 38);

	}
	else
	{
		MessageBox(hwndMDIClient, _T("Error Choose Font"), _T("Error"), 0);
	}
}

void crtToolBar(HWND hWnd)
{
	InitCommonControls();
	

	TBBUTTON tbButtons[] =
	{
		{STD_FILENEW, ID_FILE_NEW,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{STD_FILEOPEN, ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0 , 0},
		{STD_FILESAVE, ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0 , 0},
	};

	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));
	
}

void addDrawButton(HWND hWnd)
{
	TBBUTTON tbButtons[] =
	{
		{0, 0, TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0},
		{0, ID_DRAW_LINE,TBSTATE_ENABLED,TBSTYLE_BUTTON,0,0},
		{1, ID_DRAW_RECTANGLE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
		{2, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
		{3, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0	},
		{4, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0}

	};

	TBADDBITMAP tbBitmap{ hInst, IDB_BITMAP7 };

	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);
	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;
	tbButtons[5].iBitmap += idx;
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);

}


